FROM debian:unstable-slim
LABEL maintainer="Guillaume Charifi <guillaume.charifi@sfr.fr>"

RUN \
        mkdir -p /usr/share/man/man1 \
                /usr/share/man/man2  \
                /usr/share/man/man3  \
                /usr/share/man/man4  \
                /usr/share/man/man5  \
                /usr/share/man/man6  \
                /usr/share/man/man7  \
                /usr/share/man/man8  \
                && \
        apt-get -qq update && \
        apt-get upgrade -qqyy && \
        apt-get --no-install-recommends install -qqyy \
                \
                ca-certificates \
                curl            \
                wget            \
                && \
        apt-get clean && \
        rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /build /usr/share/man/*

CMD /bin/bash
